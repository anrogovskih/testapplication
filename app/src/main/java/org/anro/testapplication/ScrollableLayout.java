package org.anro.testapplication;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.OverScroller;

import java.util.ArrayList;

/**
 * Created by Анатолий on 24.08.2016.
 */
public class ScrollableLayout extends FrameLayout {
    private ImageView imageView;

    ArrayList<ButtonCoordinates> buttonCoordinatesList;

    private GestureDetectorCompat gestureDetectorCompat;
    private OverScroller overScroller;

    //these parameters will be the borders of layout scrolling
    private int layoutH = 0;
    private int layoutW = 0;

    private int positionX = 0;
    private int positionY = 0;

    public void setImageView(Bitmap imageBitmap, int id) {
        imageView.setLayoutParams(new LayoutParams(
                imageBitmap.getWidth(),
                imageBitmap.getHeight()));
        imageView.setImageBitmap(imageBitmap);
        imageView.setId(id);
    }

    public void setImageID(int imageID) {
        if (imageView != null){
            imageView.setId(imageID);
        }
        else Log.e("setImageID", "image is null");
    }

    public void setLayoutW(int layoutW) {
        this.layoutW = layoutW;
    }

    public void setLayoutH(int layoutH) {
        this.layoutH = layoutH;
    }

    public void setButtonCoordinatesList(ArrayList<ButtonCoordinates> buttonCoordinatesList) {
        this.buttonCoordinatesList = buttonCoordinatesList;
    }

    public ScrollableLayout(final Context context, Bitmap imageBitmap,
                            ArrayList<ButtonCoordinates> buttonCoordinatesList) {
        super(context);

        gestureDetectorCompat = new GestureDetectorCompat(context, gestureListener);
        overScroller = new OverScroller(context);

        imageView = new ImageView(context);
        imageView.setLayoutParams(new LayoutParams(
                imageBitmap.getWidth(),
                imageBitmap.getHeight()));
        imageView.setImageBitmap(imageBitmap);

        this.addView(imageView);

        this.buttonCoordinatesList = buttonCoordinatesList;

        for (int i = 0; i < buttonCoordinatesList.size(); i++) {
            Button button = new Button(context);
            int x = buttonCoordinatesList.get(i).getX();
            int y = buttonCoordinatesList.get(i).getY();
            if (x > imageBitmap.getWidth() || x < 0) {
                x = imageView.getDrawable().getBounds().centerX();
            }
            if (y > imageBitmap.getHeight() || y < 0) {
                y = imageView.getDrawable().getBounds().centerY();
            }
            button.setX(x);
            button.setY(y);
            button.setLayoutParams(new ScrollableLayout.LayoutParams(
                    LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
            button.setText("Button " + (i + 1));
            button.setId(i + 1);
            this.addView(button);
        }
    }

    public static class ButtonCoordinates {
        int x;
        int y;

        public ButtonCoordinates(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        gestureDetectorCompat.onTouchEvent(event);
        return true;
    }

    private int getMaxHorizontal() {
        return (Math.abs(imageView.getDrawable().getBounds().width() - layoutW));
    }

    private int getMaxVertical() {
        return (Math.abs(imageView.getDrawable().getBounds().height() - layoutH));
    }

    @Override
    public void computeScroll() {
        super.computeScroll();
        // computeScrollOffset() returns true only when the scrolling isn't
        // already finished
        if (overScroller.computeScrollOffset()) {
            positionX = overScroller.getCurrX();
            positionY = overScroller.getCurrY();
            scrollTo(positionX, positionY);
        } else {
            // when scrolling is over, we will want to "spring back" if the
            // image is overscrolled
            overScroller.springBack(positionX, positionY, 0, getMaxHorizontal(), 0, getMaxVertical());
        }
    }

    private GestureDetector.SimpleOnGestureListener gestureListener = new GestureDetector.SimpleOnGestureListener() {

        @Override
        public boolean onDown(MotionEvent e) {
            overScroller.forceFinished(true);
            ViewCompat.postInvalidateOnAnimation(ScrollableLayout.this);
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                               float velocityY) {
            overScroller.forceFinished(true);
            overScroller.fling(positionX, positionY, (int) -velocityX, (int) -velocityY, 0, getMaxHorizontal(), 0,
                    getMaxVertical());
            ViewCompat.postInvalidateOnAnimation(ScrollableLayout.this);
            return true;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2,
                                float distanceX, float distanceY) {
            overScroller.forceFinished(true);
            // normalize scrolling distances to not overscroll the image
            int dx = (int) distanceX;
            int dy = (int) distanceY;
            int newPositionX = positionX + dx;
            int newPositionY = positionY + dy;
            if (newPositionX < 0) {
                dx -= newPositionX;
            } else if (newPositionX > getMaxHorizontal()) {
                dx -= (newPositionX - getMaxHorizontal());
            }
            if (newPositionY < 0) {
                dy -= newPositionY;
            } else if (newPositionY > getMaxVertical()) {
                dy -= (newPositionY - getMaxVertical());
            }
            overScroller.startScroll(positionX, positionY, dx, dy, 0);
            ViewCompat.postInvalidateOnAnimation(ScrollableLayout.this);
            return true;
        }
    };


}
