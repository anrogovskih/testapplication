package org.anro.testapplication;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    ScrollableLayout scrollableLayout;
    ViewGroup backgroundLayout;
    ArrayList<ScrollableLayout.ButtonCoordinates> buttonCoordinatesList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            setContentView(R.layout.activity_main);
    }

    @Override
    protected void onResume() {
        super.onResume();

            backgroundLayout = (ViewGroup) findViewById(R.id.backgroundLayout);
            if (State.bitmap == null) {
                buttonCoordinatesList = new ArrayList<>();
                buttonCoordinatesList.add(new ScrollableLayout.ButtonCoordinates(30, 70));
                buttonCoordinatesList.add(new ScrollableLayout.ButtonCoordinates(130, 150));
                buttonCoordinatesList.add(new ScrollableLayout.ButtonCoordinates(300, 350));
                State.buttonCoordinatesList = buttonCoordinatesList;
                Bitmap startImgBmp = BitmapFactory.decodeResource(this.getResources(), R.drawable.image1);
                scrollableLayout = new ScrollableLayout(this,
                        startImgBmp, buttonCoordinatesList);
                setMetricsParams();
                State.imageID = "image_1".hashCode();
                scrollableLayout.setImageID(State.imageID);
                setTextVisible(State.imageID);
            } else {
                scrollableLayout = new ScrollableLayout(this,
                        State.bitmap,
                        State.buttonCoordinatesList);
                setMetricsParams();
                scrollableLayout.setImageID(State.imageID);
                setTextVisible(State.imageID);
            }
            backgroundLayout.addView(scrollableLayout);
            setPreview();
        }

    public void setMetricsParams() {
        ViewTreeObserver observer = backgroundLayout.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                scrollableLayout.setLayoutH(backgroundLayout.getHeight());
                scrollableLayout.setLayoutW(backgroundLayout.getWidth());
                backgroundLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
    }

    private void setPreview(){
        final ImageView image_1 = (ImageView) findViewById(R.id.image_1);
        setOnClickListener(image_1, R.drawable.image1, "image_1");
        final ImageView image_2 = (ImageView) findViewById(R.id.image_2);
        setOnClickListener(image_2, R.drawable.image2, "image_2");
        final ImageView image_3 = (ImageView) findViewById(R.id.image_3);
        setOnClickListener(image_3, R.drawable.image3, "image_3");
        final ImageView image_4 = (ImageView) findViewById(R.id.image_4);
        setOnClickListener(image_4, R.drawable.image4, "image_4");
        final ImageView image_5 = (ImageView) findViewById(R.id.image_5);
        setOnClickListener(image_5, R.drawable.image5, "image_5");
        final ImageView image_6 = (ImageView) findViewById(R.id.image_6);
        setOnClickListener(image_6, R.drawable.image6, "image_6");
        final ImageView image_7 = (ImageView) findViewById(R.id.image_7);
        setOnClickListener(image_7, R.drawable.image7, "image_7");
    }

    // The similar method we can use in ScrollableLayout class to set OnClickListener for buttons.
    // We just need to add another input parameter along with buttons coordinates to identify
    // which method should be invoked by clicking on concrete button.
    private void setOnClickListener (final ImageView clickedImageView,
                                     final int drawable, final String imageID){
        clickedImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                State.bitmap =
                        BitmapFactory.decodeResource(v.getContext().getResources(), drawable);
                setTextInvisible(State.imageID);
                State.imageID = imageID.hashCode();
                scrollableLayout.setImageView(State.bitmap, State.imageID);
                long time = new Date().getTime();
                setTextVisible(State.imageID);
                long newTime = new Date().getTime();
                Log.i("Time", (newTime - time) + " ");
            }
        });
    }
    private void setTextInvisible(int imageID){
        TextView text_1 = (TextView) findViewById(R.id.text_1);
        TextView text_2 = (TextView) findViewById(R.id.text_2);
        TextView text_3 = (TextView) findViewById(R.id.text_3);
        TextView text_4 = (TextView) findViewById(R.id.text_4);
        TextView text_5 = (TextView) findViewById(R.id.text_5);
        TextView text_6 = (TextView) findViewById(R.id.text_6);
        TextView text_7 = (TextView) findViewById(R.id.text_7);
        if (imageID == "image_1".hashCode()){
            text_1.setAlpha(0);
        }
        else if (imageID == "image_2".hashCode()){
            text_2.setAlpha(0);
        }
        else if (imageID == "image_3".hashCode()){
            text_3.setAlpha(0);
        }
        else if (imageID == "image_4".hashCode()){
            text_4.setAlpha(0);
        }
        else if (imageID == "image_5".hashCode()){
            text_5.setAlpha(0);
        }
        else if (imageID == "image_6".hashCode()){
            text_6.setAlpha(0);
        }
        else if (imageID == "image_7".hashCode()){
            text_7.setAlpha(0);
        }
    }

    private void setTextVisible (int imageID){
        TextView text_1 = (TextView) findViewById(R.id.text_1);
        TextView text_2 = (TextView) findViewById(R.id.text_2);
        TextView text_3 = (TextView) findViewById(R.id.text_3);
        TextView text_4 = (TextView) findViewById(R.id.text_4);
        TextView text_5 = (TextView) findViewById(R.id.text_5);
        TextView text_6 = (TextView) findViewById(R.id.text_6);
        TextView text_7 = (TextView) findViewById(R.id.text_7);
        if (imageID == "image_1".hashCode()){
            text_1.setAlpha(1);
        }
        else if (imageID == "image_2".hashCode()){
            text_2.setAlpha(1);
        }
        else if (imageID == "image_3".hashCode()){
            text_3.setAlpha(1);
        }
        else if (imageID == "image_4".hashCode()){
            text_4.setAlpha(1);
        }
        else if (imageID == "image_5".hashCode()){
            text_5.setAlpha(1);
        }
        else if (imageID == "image_6".hashCode()){
            text_6.setAlpha(1);
        }
        else if (imageID == "image_7".hashCode()){
            text_7.setAlpha(1);
        }
    }

}
